## Install Dependencies:

On debian based systems, do a 

  apt-get install gcc libalut-dev
  apt-get install libsdl-image1.2-dev

This will probably install every necessary dependency like openal-dev, sdl-dev and so on.

## How to compile:

gcc gameClient.c  -lopenal -lalut  -lSDL_image -lSDL -o Shooter
