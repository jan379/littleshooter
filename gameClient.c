#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "gamedefs.h"
#include "shoot.c"
#include "sound.h"
#include "sound.c"

/*SDL stuff*/
SDL_Surface *screen, *player, *opponent, *ball, *health, *healthPlayer, *healthOpp;
SDL_Rect src, dest;
SDL_Event event;
Uint8 *keys;
int done = 0; /* switch for terminating the game loop */
int shootDelay = 0;

struct coord playerCoord, opponentCoord; /* DEFINITION */



int graphicsInit(){

	printf("Initialize graphics\n");
	
	/*load graphics into buffer*/
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		printf("Kann SDL nicht initialisieren: %s\n", SDL_GetError());
		return 1;
	}

	/*Sicherstellen, dass SDL beendet wird bei Programmende.*/
	atexit(SDL_Quit);

	/*try to set videomode according to defined values at gamedefs.h at 8 bit. */
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 8, SDL_HWSURFACE);
	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0,0));
	if (screen == NULL) {
		printf("Cannot set videomode: %s\n", SDL_GetError());
		return 1;
	}

	printf("loading image files...\n");

	player = IMG_Load("./graphics/player.png");
	if (player == NULL) {
		printf("Not able to load graphics for player.\n");
		return 1;
	}
	opponent = IMG_Load("./graphics/opponent.png");
	if (opponent == NULL) {
		printf("Not able to load graphics for opponent.\n");
		return 1;
	}
	ball = IMG_Load("./graphics/ball.png");
        if (ball == NULL) {
                printf("Not able to load bullet graphics.\n");
		return 1;
	}
	health = IMG_Load("./graphics/healthBar.png");
        if (health == NULL) {
                printf("Not able to load graphics for playerHealth.\n");
		return 1;
	}
	healthPlayer = IMG_Load("./graphics/healthPlayer.png");
        if (healthPlayer == NULL) {
                printf("Not able to load graphics for healthPlayer.\n");
		return 1;
	}
	healthOpp = IMG_Load("./graphics/healthOpponent.png");
        if (healthOpp == NULL) {
                printf("Not able to load graphics for healthOpponent.\n");
		return 1;
	}
}
struct coord playerDead(struct coord player){
	int dead = player.ID;
	printf("player %d died.\n", dead);
	return player;
}
struct coord playerControl(struct coord temp){

	temp = collDetBalls(temp);
	/*First Attempt to insert CollisionDetection*/

	blitHealth(temp, SCREEN_WIDTH - MARGIN - health->w, MARGIN);
	/*prints players health on screen*/

	if(temp.health <= 0){
		playerDead(temp);
		temp.health = 100;
	}

	if(temp.y < SCREEN_HEIGHT - temp.h){
		temp.y++;
	}
	if(keys[SDLK_UP]){
		if(temp.y > 0){ 
			temp.y -= SPEED;
			alSourcePlay(Sources[ENGINE1]);
		}
	}
	if(keys[SDLK_DOWN]){
		if(temp.y < SCREEN_HEIGHT - temp.h){
			temp.y += SPEED;
		}
	}

	if(keys[SDLK_LEFT]){
		if(temp.x > 0){
			temp.x -= SPEED;
			temp.direction = 0;
			temp.clipping = temp.w;
		}
		else{
			temp.direction = 0;
			temp.clipping = temp.w;
		}
	}
	if(keys[SDLK_RIGHT]){
		if(temp.x < SCREEN_WIDTH - temp.w){
			temp.x += SPEED;
			temp.direction = 1;
			temp.clipping = 0;
		}
		else{
			temp.direction = 1;
			temp.clipping = 0;
		}
	}	
	if (keys[SDLK_RCTRL]){ 
	/* shoot-button pressed */
	if (temp.shootDelay == 1  && temp.shoot < MAXSHOOTS / 2 ){ 
		/* shoot */
			/* shootnumber must be smaller than 20 !! */		
			boom[shootnumber] = createBoom(temp);
			shootnumber++;
			temp.shoot++;
			alSourcePlay(Sources[RUMBLE1]);	
		}
		if (temp.shoot >= MAXSHOOTS / 2) 
			temp.shoot = 0;
		/* don't shoot for delay */ 
		if (temp.shootDelay >= 10){
			temp.shootDelay = 0;
		}
		if (temp.shootDelay < 10){
			temp.shootDelay++;
		}
	
	}
	else {
	/* shoot-button NOT pressed */
	shootDelay = 0;
	}
	return temp;
}

struct coord oppLocal(struct coord temp){

	temp = collDetBalls(temp); 
	/*tests for collision with balls*/

	blitHealth(temp, MARGIN, MARGIN); 
	/*prints players health on screen*/

	if(temp.health <= 0){
		playerDead(temp);
		temp.health = 100;
	}

	if(temp.y < SCREEN_HEIGHT - temp.h){
		temp.y++;
	}

	if(keys[SDLK_w]){
		if(temp.y > 0){ 
			temp.y -= SPEED;
			alSourcePlay(Sources[ENGINE2]);
		}
	}
	if(keys[SDLK_s]){
		if(temp.y < SCREEN_HEIGHT - temp.h){
			temp.y += SPEED;
		}
	}

	if(keys[SDLK_a]){
		if(temp.x > 0){
			temp.x -= SPEED;
			temp.direction = 0;
			temp.clipping = temp.w;
		}
		else{
			temp.direction = 0;
			temp.clipping = temp.w;
		}
	}
	if(keys[SDLK_d]){
		if(temp.x < SCREEN_WIDTH - temp.w){
			temp.x += SPEED;
			temp.direction = 1;
			temp.clipping = 0;
		}
		else{
			temp.direction = 1;
			temp.clipping = 0;
		}
	}	
	if (keys[SDLK_SPACE]){ 
	/* shoot-button pressed */
	if (temp.shootDelay == 1  && temp.shoot < MAXSHOOTS / 2 ){ 
		/* shoot */
			/* shootnumber must be smaller than 20 !! */		
			boom[shootnumber] = createBoom(temp);
			shootnumber++;
			temp.shoot++;
			alSourcePlay(Sources[RUMBLE2]);	
		}
		if (temp.shoot >= MAXSHOOTS / 2) 
			temp.shoot = 0;
		/* don't shoot for delay */ 
		if (temp.shootDelay >= 10){
			temp.shootDelay = 0;
		}
		if (temp.shootDelay < 10){
			temp.shootDelay++;
		}
	
	}
	else {
	/* shoot-button NOT pressed */
	shootDelay = 0;
	}
	return temp;
}


int blitHealth(struct coord parent, int x, int y){
	int clipping = (health->w / 100) * parent.health;
	SDL_SetAlpha(health, SDL_SRCALPHA, 128);
	src.w = clipping;
	src.h = health->h;
	src.y = 0;
	src.x = 0;
	dest.w = src.w;
	dest.h = src.h;
	dest.x = x;
	dest.y = y;
	SDL_BlitSurface(health, &src, screen, &dest);

	if (parent.ID == 0){
		SDL_SetAlpha(health, SDL_SRCALPHA, 128);
		dest.w = src.w;
		dest.h = src.h;
		dest.x = x;
		dest.y = y + health->h;
		SDL_BlitSurface(healthPlayer, NULL, screen, &dest);
	}
	if (parent.ID == 1){
		SDL_SetAlpha(health, SDL_SRCALPHA, 128);
		dest.w = src.w;
		dest.h = src.h;
		dest.x = x;
		dest.y = y + health->h;
		SDL_BlitSurface(healthOpp, NULL, screen, &dest);
	}

}
int blitBall(int x, int y){
	SDL_SetAlpha(ball, SDL_SRCALPHA, 128);
	dest.w = ball->w; 
	dest.h = ball->h;
	dest.x = x;
	dest.y = y;
	SDL_BlitSurface(ball, 0, screen, &dest);
}
int blitControl(){

	SDL_SetAlpha(player, SDL_SRCALPHA, 128);
	src.w = player->w/2;
	src.h = player->h;
	src.y = 0;
	src.x = playerCoord.clipping;
	dest.w = src.w;
	dest.h = src.h;
	dest.x = playerCoord.x;
	dest.y = playerCoord.y;
	SDL_BlitSurface(player, &src, screen, &dest);

	SDL_SetAlpha(opponent, SDL_SRCALPHA, 128);
	src.w = opponent->w/2;
	src.h = opponent->h;
	src.y = 0;
	src.x = opponentCoord.clipping;
	dest.w = src.w;
	dest.h = src.h;
	dest.x = opponentCoord.x;
	dest.y = opponentCoord.y;
	SDL_BlitSurface(opponent, &src, screen, &dest);

}
main(){

graphicsInit();
SDL_WM_SetCaption("I want blood!", "It's a shooter!");
soundInit();
SDL_ShowCursor(0);

playerCoord.x = 0 + player->w;
playerCoord.y = screen->h - player->h;
playerCoord.w = player->w/2;
playerCoord.h = player->h;
playerCoord.direction = 1;
playerCoord.ID = 0;
playerCoord.health = 100;

opponentCoord.x = screen->w - opponent->w;
opponentCoord.y = screen->h - opponent->h;
opponentCoord.w = opponent->w/2;
opponentCoord.h = opponent->h;
opponentCoord.direction = 0;
opponentCoord.ID = 1;
opponentCoord.health = 100;


printf("Mainloop starts now.\n");

while (!done) {

	while (SDL_PollEvent(&event)) {
        	switch(event.type) {
	        case SDL_QUIT:
		done = 1;
		break;
		}
	}
	keys = SDL_GetKeyState(NULL);
	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 123, 123, 250));
	playerCoord = playerControl(playerCoord);
	opponentCoord = oppLocal(opponentCoord);
	updateShoots();
	blitControl();

	if(keys[SDLK_ESCAPE]){
		done = 1;
		}


	/*SDl screen-update:*/

	SDL_UpdateRect(screen, 0, 0, 0, 0); 
	SDL_Delay(5);

} 	/*ending while-loop*/

/*Den fuer das Bild reservierten Speicher freigeben: */
SDL_FreeSurface(player);
SDL_FreeSurface(opponent);
SDL_FreeSurface(ball);
SDL_FreeSurface(screen);
KillALData();
return 0;
}



