#ifndef GAMEDEFS_H
#define GAMEDEFS_H

#define  SCREEN_WIDTH  800 
#define  SCREEN_HEIGHT  600
#define  MARGIN 30
#define  SPEED  10


#define MAXSHOOTS 20
#define TTL 30 		/*time to live for every single shoot.*/

struct shoot{
	int ttl;	/*a shoot should not exist for ever*/
	int x;		
	int y;
	int direction;
	int ID;		/*could be used to bind different properties to 
			  different players e.g. their shoots*/
};

struct coord{
        int x;
        int y;
	int w;
	int h;
	int direction;
	int health;
	int points;
	int clipping;
	int ID;
	int shoot;
	int shootDelay;
};

struct shoot boom[MAXSHOOTS];

#endif 
