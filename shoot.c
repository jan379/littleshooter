#include "sound.h"

/*playerCoords should have built in direction
  because they have to pass it through to 
  their bullets*/



/*enough bullets for all players*/


/*PROTOTYPES FOR SHOOTING*/

struct shoot createBoom(struct coord parentCoord);

int updateShoots();

struct coord collDetBalls(struct coord initiator);

/*definitions*/

int shootnumber = 0;

struct shoot createBoom(struct coord parent){
	struct shoot child;
	child.ttl = TTL;
	child.x = parent.x;
	child.y = parent.y;
	child.direction = parent.direction;
	child.ID = parent.ID;
	return child;
}






int updateShoots(){
	if (shootnumber >= 0){
		int i;
		for(i = 0; i < shootnumber; i++){

			if(boom[i].direction == 0){		/*zero means left*/
				if(boom[i].ttl > 0){ 	/* boom[int] defined in gamedefs */	
					boom[i].ttl--;
					boom[i].x -= (SPEED*2);
					blitBall(boom[i].x, boom[i].y);
				}
				else{
					boom[i].x = SCREEN_WIDTH * 2;	/*boom is not longer on screen*/
					boom[i].y = SCREEN_HEIGHT * 2;
				}
			}
			if(boom[i].direction == 1){		/*one means right*/
				if(boom[i].ttl > 0){ 	
					boom[i].ttl--;
					boom[i].x += (SPEED*2);
					blitBall(boom[i].x, boom[i].y);

				}
				else{
					boom[i].x = SCREEN_WIDTH * 2;
					boom[i].y = SCREEN_HEIGHT * 2;
				}

			}
		}
	}
	if (shootnumber < 0){
		shootnumber = 0; /* FIXME: check if needed */
		printf("FIXME\n");
	}
	if (shootnumber >= MAXSHOOTS - 2){	/*-2 is for security reasons.*/
		shootnumber = 0;		/*it can be, that both players */
	}					/*increase the shootnumber, so its more*/
}						/*safe this way.*/

struct coord collDetBalls(struct coord parent){
	if (shootnumber >= 0){
		int i;
		for(i = 0; i < shootnumber; i++){
			if(parent.x < boom[i].x){	/*this means, ball comes from right*/
				if(	(boom[i].x - parent.x) <= 3 
					&& (boom[i].y - parent.y) <= 30 
					&& (boom[i].y - parent.y) >= 0 
					&& boom[i].ID != parent.ID){	
					parent.health -= 10;
					boom[i].ttl = 0;
					printf("Hit\n");
					alSourcePlay(Sources[SUCCES]);
					printf("Player %d has been hit.\n", parent.ID);
					printf("Player %d health: %d\n", parent.ID, parent.health);
				}
			}

			if(parent.x > boom[i].x){	/*this means, ball comes from left*/
				if(	(parent.x - boom[i].x) <= 30 
					&& (boom[i].y - parent.y) <= 30
					&& (boom[i].y - parent.y) >= 0 	
					&& boom[i].ID != parent.ID){	
					parent.health -= 10;
					boom[i].ttl = 0;
					printf("Hit\n");
					alSourcePlay(Sources[SUCCES]);
					printf("Player %d has been hit.\n", parent.ID);
					printf("Player %d health: %d\n", parent.ID, parent.health);
				}
			}
		}
	}
	return parent;
}

