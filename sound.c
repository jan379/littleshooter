#include "sound.h"
/*==============================*/
/*Sound*/
/*==============================*/



ALboolean LoadALData(){

	/* Variables to load into.*/

	ALenum format;
	ALsizei size;
	ALvoid* data;
	ALsizei freq;
	ALboolean loop;

	/* Load wav data into a buffer.*/

	alGenBuffers(NUM_BUFFERS, Buffers);

	if(alGetError() != AL_NO_ERROR)
		return AL_FALSE;

	Buffers[RUMBLE2] = alutCreateBufferFromFile("wavdata/rumble2.wav");
	Buffers[RUMBLE1] = alutCreateBufferFromFile("wavdata/rumble1.wav");
	Buffers[SUCCES]  = alutCreateBufferFromFile("wavdata/succes.wav");
	Buffers[ENGINE1]  = alutCreateBufferFromFile("wavdata/engine1.wav");
	Buffers[ENGINE2]  = alutCreateBufferFromFile("wavdata/engine2.wav");
	/* Bind the buffer with the source.*/

	alGenSources(NUM_SOURCES, Sources);

	if(alGetError() != AL_NO_ERROR)
		return AL_FALSE;




 	alSourcei (Sources[RUMBLE2], AL_BUFFER,   Buffers[RUMBLE2]   );
        alSourcef (Sources[RUMBLE2], AL_PITCH,    1.0f              );
        alSourcef (Sources[RUMBLE2], AL_GAIN,     1.0f              );
        alSourcefv(Sources[RUMBLE2], AL_POSITION, SourcesPos[RUMBLE2]);
        alSourcefv(Sources[RUMBLE2], AL_VELOCITY, SourcesVel[RUMBLE2]);
        alSourcei (Sources[RUMBLE2], AL_LOOPING,  AL_FALSE          );

        alSourcei (Sources[RUMBLE1], AL_BUFFER,   Buffers[RUMBLE1]   );
        alSourcef (Sources[RUMBLE1], AL_PITCH,    1.0f            );
        alSourcef (Sources[RUMBLE1], AL_GAIN,     1.0f            );
        alSourcefv(Sources[RUMBLE1], AL_POSITION, SourcesPos[RUMBLE1]);
        alSourcefv(Sources[RUMBLE1], AL_VELOCITY, SourcesVel[RUMBLE1]);
        alSourcei (Sources[RUMBLE1], AL_LOOPING,  AL_FALSE        );

        alSourcei (Sources[SUCCES], AL_BUFFER,   Buffers[SUCCES]   );
        alSourcef (Sources[SUCCES], AL_PITCH,    1.0f            );
        alSourcef (Sources[SUCCES], AL_GAIN,     1.0f            );
        alSourcefv(Sources[SUCCES], AL_POSITION, SourcesPos[SUCCES]);
        alSourcefv(Sources[SUCCES], AL_VELOCITY, SourcesVel[SUCCES]);
        alSourcei (Sources[SUCCES], AL_LOOPING,  AL_FALSE        );

	alSourcei (Sources[ENGINE1], AL_BUFFER,   Buffers[ENGINE1]   );
        alSourcef (Sources[ENGINE1], AL_PITCH,    1.0f            );
        alSourcef (Sources[ENGINE1], AL_GAIN,     0.3f            );
        alSourcefv(Sources[ENGINE1], AL_POSITION, SourcesPos[ENGINE1]);
        alSourcefv(Sources[ENGINE1], AL_VELOCITY, SourcesVel[ENGINE1]);
        alSourcei (Sources[ENGINE1], AL_LOOPING,  AL_FALSE        );

	alSourcei (Sources[ENGINE2], AL_BUFFER,   Buffers[ENGINE2]   );
        alSourcef (Sources[ENGINE2], AL_PITCH,    1.0f            );
        alSourcef (Sources[ENGINE2], AL_GAIN,     0.3f            );
        alSourcefv(Sources[ENGINE2], AL_POSITION, SourcesPos[ENGINE2]);
        alSourcefv(Sources[ENGINE2], AL_VELOCITY, SourcesVel[ENGINE2]);
        alSourcei (Sources[ENGINE2], AL_LOOPING,  AL_FALSE        );


	/* Do another error check and return.*/

	if(alGetError() == AL_NO_ERROR){
		return AL_TRUE;
		printf("Soundfiles loaded...\n");
	}
	return AL_FALSE;
}

void SetListenerValues()
{
	alListenerfv(AL_POSITION,    ListenerPos);
	alListenerfv(AL_VELOCITY,    ListenerVel);
	alListenerfv(AL_ORIENTATION, ListenerOri);
}

void KillALData()
{
	alDeleteBuffers(NUM_BUFFERS, Buffers);
	alDeleteSources(NUM_SOURCES, Sources);
	alutExit();
}


int soundInit(){  

	/* Initialize OpenAL and clear the error bit.*/

	alutInit(NULL, 0);
	alGetError();

	/* Load the wav data.*/
	if(LoadALData() == AL_FALSE)
		{
	    	printf("Error loading data.");
			return 0;
		}
	
	SetListenerValues();

	/* SetupOne an exit procedure.*/

	atexit(KillALData);
	
	printf("Sound initialized...\n");
}
